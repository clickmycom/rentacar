<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>{{$txt_title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->

    <link href="{{asset("assets/admin/plugins/pace/pace-theme-flash.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/boostrapv3/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/font-awesome/css/font-awesome.css") }}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.css") }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/plugins/bootstrap-select2/select2.css") }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/plugins/switchery/css/switchery.min.css") }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/pages/css/pages-icons.css") }}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{asset("assets/admin/pages/css/pages.css") }}" rel="stylesheet" type="text/css" />


    <!-- END  MANDATORY STYLE -->
    <script src="{{asset("assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js")}}"></script>
</head>

<body class="fixed-header   mac desktop pace-done menu-pin">


<!-- BEGIN SIDEBPANEL-->
@include("$this_system/layouts.slide_panel")
<!-- END SIDEBPANEL-->


<!-- START PAGE-CONTAINER -->
<div class="page-container ">
    <!-- START HEADER -->
    @include("$this_system/layouts.Header")
    <!-- END HEADER -->

    <!-- START PAGE CONTENT WRAPPER -->
    <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
            <!-- START JUMBOTRON -->
            <div class="jumbotron" data-pages="parallax">
                <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                    <div class="inner">
                        @include("{$content}")
                    </div>
                </div>
            </div>
            <!-- END JUMBOTRON -->
            <!-- START CONTAINER FLUID -->
            <div class="container-fluid container-fixed-lg">
                <!-- BEGIN PlACE PAGE CONTENT HERE -->
                <!-- END PLACE PAGE CONTENT HERE -->
            </div>
            <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->

    </div>
    <!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->














<!-- BEGIN VENDOR JS -->
<script src="{{asset("assets/admin/plugins/pace/pace.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery/jquery-1.11.1.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/modernizr.custom.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-ui/jquery-ui.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/boostrapv3/js/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery/jquery-easy.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-unveil/jquery.unveil.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-bez/jquery.bez.min.js")}}"></script>
<script src="{{asset("assets/admin/plugins/jquery-ios-list/jquery.ioslist.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-actual/jquery.actual.min.js")}}"></script>
<script src="{{asset("assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-select2/select2.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/classie/classie.js")}}"></script>
<script src="{{asset("assets/admin/plugins/switchery/js/switchery.min.js")}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset("assets/admin/pages/js/pages.min.js")}}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{asset("assets/admin/js/scripts.js")}}" type="text/javascript"></script>
<!-- END PAGE LEVEL JS -->


</body>
</html>
