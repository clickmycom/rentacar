<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Pages - Admin Dashboard UI Kit - Lock Screen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{asset("assets/admin/plugins/pace/pace-theme-flash.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/boostrapv3/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/font-awesome/css/font-awesome.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{asset("assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.css")}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/plugins/bootstrap-select2/select2.css")}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/plugins/switchery/css/switchery.min.css")}}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{asset("assets/admin/pages/css/pages-icons.css")}}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{asset("assets/admin/pages/css/pages.css")}}" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="{{asset("assets/admin/pages/css/ie9.css")}}" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header ">
<div class="register-container full-height sm-p-t-30">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <img src="{{asset("assets/img/file_logo.png")}}" alt="logo" data-src="assets/img/file_logo.png" data-src-retina="assets/img/file_logo.png">
                <h3>Register Form</h3>

                <form class="p-t-15"  id="form-register"  role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label>First Name</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group form-group-default  {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label>Last Name</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default  {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label>Telephone</label>
                                <span class="help">e.x. "(081) 234-5678"</span>
                                <input type="text" id="phone" name="phone" value="{{ old('phone') }}" class="form-control">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            <div class="col-sm-6">
                                <div class=" form-group-default" style="padding-bottom: 15px;">
                                    <label><strong>Sex</strong></label>

                                    <div class="radio radio-success">
                                        <div class="col-sm-3">
                                            <input type="radio" value="male" name="sex" id="male">
                                            <label for="male">Male</label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" value="female" name="sex" id="female">
                                            <label for="female">female</label>
                                        </div>
                                    </div>
                                </div>
                            </div>


                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default  {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label>Email</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Minimum of 6 Charactors" class="form-control" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label>Re-Password</label>
                                <input type="password" name="password_confirmation" placeholder="" class="form-control" required>
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>







                    <button class="btn btn-primary btn-cons m-t-10" type="submit">Create a new account</button>
                    <a class="btn btn-danger btn-cons m-t-10 pull-right"  href="{{ URL::previous() }}">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- BEGIN VENDOR JS -->
<script src="{{asset("assets/admin/plugins/pace/pace.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery/jquery-1.11.1.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/modernizr.custom.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-ui/jquery-ui.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/boostrapv3/js/bootstrap.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery/jquery-easy.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-unveil/jquery.unveil.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-bez/jquery.bez.min.js")}}"></script>
<script src="{{asset("assets/admin/plugins/jquery-ios-list/jquery.ioslist.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-actual/jquery.actual.min.js")}}"></script>
<script src="{{asset("assets/admin/plugins/jquery-scrollbar/jquery.scrollbar.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-select2/select2.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/classie/classie.js")}}"></script>
<script src="{{asset("assets/admin/plugins/switchery/js/switchery.min.js")}}" type="text/javascript"></script>
<script src="{{asset("assets/admin/plugins/jquery-validation/js/jquery.validate.min.js")}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-tag/bootstrap-tagsinput.min.js")}}" ></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/jquery-inputmask/jquery.inputmask.min.js")}}" ></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/jquery-autonumeric/autoNumeric.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/jquery-validation/js/jquery.validate.min.js")}}" ></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/summernote/js/summernote.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/moment/moment.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-daterangepicker/daterangepicker.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/js/form_elements.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/dropzone/dropzone.min.js")}}"></script>


<script type="text/javascript" src="{{asset("assets/admin/plugins/classie/classie.js")}}"></script>
<script src="{{asset("assets/admin/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/bootstrap-tag/bootstrap-tagsinput.min.js")}}"></script>
<script type="text/javascript" src="{{asset("assets/admin/plugins/jquery-inputmask/jquery.inputmask.min.js")}}"></script>
<script src="{{asset("assets/admin/plugins/boostrap-form-wizard/js/jquery.bootstrap.wizard.min.js")}}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<script src="{{asset("assets/admin/pages/js/pages.min.js")}}"></script>
<script>
    $(function()
    {
        $('#form-register').validate()
    })
</script>
</body>
</html>