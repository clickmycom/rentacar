<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->this_system  = 'back-end/';
        $this->middleware('auth:admin');
    }
  
    public function index(){
       
        $data['this_system']    =   $this->this_system;
        $data['txt_title']          =   "Dashboard";
        $data['content']              =   $this->this_system."Dashboard.Index";
        
      return view("$this->this_system"."Layouts.Home")->with($data);
    }

}
